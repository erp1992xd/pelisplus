import { ObjectId } from "mongodb";

export default class Producto {

    constructor(
        public nombre: string,
        public precio: number,
        public stock: number,
        public _id?: ObjectId,
    ) { }
}